from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

print(PEXELS_API_KEY)


def get_pic_url(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    my_params = {'query': f"{city}, {state}"}
    url = 'https://api.pexels.com/v1/search'
    response = requests.get(url, headers=headers, params= my_params)
    data = json.loads(response.text)
    photo = data['photos'][0]['src']['original']
    return photo


def get_weather_data(city, state):
    location_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    location_response = requests.get(location_url)
    location_data = json.loads(location_response.text)
    lat = location_data[0]['lat']
    lon = location_data[0]['lon']

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.text)
    description = weather_data['weather'][0]['description']
    temperature = weather_data['main']['temp']
    return {"temp": temperature, "description": description}
